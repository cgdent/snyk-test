#!/bin/sh

set -e -x

mkdir -p /builds/curben/snyk-test/public/

STATUS=$(cat "/builds/curben/snyk-test/.status")

if [ "$STATUS" -eq "Success" ]; then
  wget https://img.shields.io/badge/pipeline-passed-success.svg -O /builds/curben/snyk-test/public/status.svg
else
  wget https://img.shields.io/badge/pipeline-failed-critical.svg -O /builds/curben/snyk-test/public/status.svg
fi

